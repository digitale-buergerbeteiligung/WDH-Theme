<!DOCTYPE html>

<#include init />

<html class="${root_css_class}" dir="<@liferay.language key=" lang.dir " />" lang="${w3c_language_id}">

<head>
  <title>${the_title} - ${company_name}</title>

  <meta content="initial-scale=1.0, width=device-width" name="viewport" />

  <meta name="description" content="Sie haben eine Idee und möchten daraus ein Projekt für die Bürgerinnen und Bürger von Kassel entwickeln? Dann erstellen Sie jetzt Ihr eigenes Projekt!">
  <meta name="keywords" content="Hessen smart gemacht, Ideen, Projekte, Bürgerinnen, Bürger, Dienstleistung, Plattform, Portal, Leimeister, Wirtschaftsinformatik">


  <@liferay_util["include"] page=top_head_include />
</head>

<body class="${css_class}">

  <@liferay_ui["quick-access"] contentId="#main-content" />

  <@liferay_util["include"] page=body_top_include />

  <@liferay.control_menu />

  <div id="wrapper">
    <header class="" id="banner" role="banner">
      <div class="row">
        <div class="navbar-header" id="heading">
          <div class="logo-container">
            <a class="${logo_css_class}" href="${site_default_url}">
              <img alt="Logo" src="${site_logo}" />
            </a>

            <#if show_site_name>
              <span class="site-name" title="<@liferay.language_format arguments=" ${site_name} " key=" go-to-x " />">
                ${site_name}
              </span>
            </#if>
          </div>

          <#include "${full_templates_path}/navigation.ftl" />

          <div class="menu-right">
            <#if is_setup_complete>
              <div class="pull-right user-personal-bar">
                <@liferay.user_personal_bar />
              </div>
              <#if is_signed_in>
                <div class="additionalMenu">
                  <a href="/web/guest/projekt-einreichen" class="btn red placeproject" role="menuitem" tabindex="0"><span>Projekt-Einreichen</span></a>
                  <a href="/web/guest/meineprojekte" role="menuitem" tabindex="1"><span>Meine Projekte</span></a>
                </div>
              </#if>
              <div class="navbar-form navbar-right" role="search">
                <@liferay.search default_preferences="${freeMarkerPortletPreferences}" />
              </div>
              <button aria-controls="navigation" aria-expanded="false" class="collapsed navbar-toggle" data-target="#navigationCollapse" data-toggle="collapse" type="button">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
            </#if>
          </div>
        </div>
      </div>
    </header>

    <section class="container-fluid-1280" id="content">
      <h1 class="hide-accessible">${the_title}</h1>

      <#if selectable>
        <@liferay_util["include"] page=content_include />
        <#else>
          ${portletDisplay.recycle()} ${portletDisplay.setTitle(the_title)}

          <@liferay_theme["wrap-portlet"] page="portlet.ftl">
            <@liferay_util["include"] page=content_include />
            </@>
      </#if>
    </section>

    <footer class="container-fluid-1280" id="footer" role="contentinfo">
      <div class="row">
        <div class="footer-wrapper">
          <div class="left">
            <p style="display:inline;"><img src="/documents/20142/32730/UNI-KS-WI-JML-Logo.jpg/3b7cee7d-d94c-ed54-f706-238e2feb6732?t=1615453669816" style="height: 58px; width: 240px;">&nbsp;  ©  </p>&nbsp;<p id="ausgabe" style="display:inline;"></p>
            <script>var jahr = new Date().getFullYear()
              document.getElementById('ausgabe').innerHTML = jahr;</script>
          </div>
          <div class="middle"><a href="/ueber">Über</a> <a href="/faq">FAQ</a> <a href="/agb">AGB</a> <a href="/impressum">Impressum</a> <a href="/datenschutz">Datenschutz</a></div>
          <div class="right"><a href="https://www.facebook.com/InformationSystems/" target="_blank"><i class="icon-facebook"></i></a> <a href="https://www.instagram.com/UniKassel/" target="_blank"><i class="icon-instagram"></i></a> <a href="https://twitter.com/leimeisterwinfo"
                                                                                                                                                                                                                                           target="_blank"><i class="icon-twitter"></i></a> <a href="https://www.youtube.com/UniKassel/" target="_blank"><i class="icon-youtube"></i></a></div>
        </div>
      </div>
    </footer>

  </div>

  <@liferay_util["include"] page=body_bottom_include />

  <@liferay_util["include"] page=bottom_include />

</body>

</html>